//
//  TransactionTableViewCell.swift
//  ChocoBitcoin
//
//  Created by Dima on 11/16/18.
//  Copyright © 2018 fime. All rights reserved.
//

import UIKit

class TransactionTableViewCell: UITableViewCell {

    @IBOutlet weak var typeLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var amountLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.selectionStyle = .none
    }
    
    func setup(item: Transaction){
        if item.type == 0{ //buy
            self.typeLabel.text = "Buy"
            self.priceLabel.textColor = AppSettings.current.sale
            self.amountLabel.textColor = AppSettings.current.income
        }
        else{ //sell
            self.typeLabel.text = "Sell"
            self.priceLabel.textColor = AppSettings.current.income
            self.amountLabel.textColor = AppSettings.current.sale
        }
        
        self.dateLabel.text = item.date
        self.priceLabel.text = item.calculated.currencyFormat(symbol: "$")
        self.amountLabel.text = "\(item.amount!) BTC"
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
