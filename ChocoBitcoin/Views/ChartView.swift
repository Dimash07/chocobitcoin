//
//  ChartView.swift
//  ChocoBitcoin
//
//  Created by Dima on 11/15/18.
//  Copyright © 2018 fime. All rights reserved.
//

import UIKit
import LMGraphView

class ChartView: UIView {

    var graph: LMLineGraphView!
    
    let strokeColor = UIColor.white
    let fillColor = UIColor(rgbValue: 0xFFFFFF, alpha: 0.2)
    let graphPointColor = UIColor.white
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
    }
    
    func createChart()->LMLineGraphView{
        
        let graph = LMLineGraphView(frame: CGRect(x: 0, y: 0, width: self.bounds.width, height: self.bounds.height))
        
        graph.layout.yAxisLabelColor = .white
        graph.layout.yAxisGridHidden = true
        graph.layout.yAxisZeroHidden = true
        graph.layout.yAxisSegmentCount = 10
        
        return graph
    }
    
    func drawItems(_ items: [CurrencyValue]){
        
        if self.graph != nil{
            self.graph.removeFromSuperview()
        }
        
        self.graph = self.createChart()
        
        
        var maxValue: Double = 0
        
        let plot = LMGraphPlot()
        plot.strokeColor = self.strokeColor
        plot.fillColor = self.fillColor
        plot.graphPointColor = self.graphPointColor
        
        var xAxisItems = [[Int: String]]()
        var graphPoints = [LMGraphPoint]()
        
        for (index, current) in items.enumerated(){
            
            let item: [Int: String] = [index+1: ""]
            xAxisItems.append(item)
            
            graphPoints.append(LMGraphPointMake(CGPoint(x: index+1, y: Int(current.value)), "", "\(current.date)\n\(current.value.currencyFormat())"))
            
            if maxValue < current.value{
                maxValue = current.value
            }
            
        }
        
        self.graph.yAxisMaxValue = CGFloat(maxValue*1.2)
        self.graph.xAxisValues = xAxisItems
        
        plot.graphPoints = graphPoints
        self.graph.graphPlots = [plot]
        
        
        
        self.addSubview(self.graph)
        
    }
    
}
