//
//  API.swift
//  ChocoBitcoin
//
//  Created by Dima on 11/15/18.
//  Copyright © 2018 fime. All rights reserved.
//

import Foundation
import Alamofire
import AlamofireObjectMapper

struct API{
    
    typealias CurrencyResponse = (_ result: Currency?) -> Void
    typealias TransactionsResponse = (_ result: [Transaction]?) -> Void
    
    static public func getCurrentPrice(currency: CurrenciesType, _ completion: @escaping CurrencyResponse){
        
        let today = Date()
        let yearAgo = Calendar.current.date(
            byAdding: .year,
            value: -1,
            to: today
        )
        
        
        let formatter = DateFormatter()
        
        formatter.dateStyle = .long
        formatter.timeStyle = .short
        formatter.dateFormat = "yyyy-MM-dd"
        
        let URL = "https://api.coindesk.com/v1/bpi/historical/\(ChocoHelper.current.defaultCrypto).json"
        
        let params = [
            "currency": currency.rawValue,
            "start": formatter.string(from: yearAgo!),
            "end": formatter.string(from: today)
        ]
        
        Alamofire.request(URL, parameters: params).responseObject { (response: DataResponse<Currency>) in
            if let result = response.result.value {
                completion(result)
            }
            else{
                completion(nil)
            }
        }
        
    }
    
    
    static public func getTransactions(_ completion: @escaping TransactionsResponse){
        
        let URL = "https://www.bitstamp.net/api/transactions"
        
        Alamofire.request(URL).responseArray { (response: DataResponse<[Transaction]>) in
            if let result = response.result.value {
                completion(result)
            }
            else{
                completion(nil)
            }
        }
        
        
    }
        
}

