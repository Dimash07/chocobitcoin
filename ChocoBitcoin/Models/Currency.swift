//
//  Currency.swift
//  ChocoBitcoin
//
//  Created by Dima on 11/15/18.
//  Copyright © 2018 fime. All rights reserved.
//

import Foundation
import ObjectMapper

public class Currency: Mappable{
    
    var values: [String: Double]?
    
    var currentValue: CurrencyValue?
    var symbol: String!
    
    var items = [CurrencyValue]()
    
    
    var yearItems = [CurrencyValue]() //average month values
    var monthItems = [CurrencyValue]() //average week values
    var weekItems = [CurrencyValue]()
    
    required convenience public init?(map: Map) {
        self.init()
    }
    
    public func mapping(map: Map) {
        values <- map["bpi"]
        
        prepareResult()
    }
    
    
    func prepareResult(){
        
        if self.values != nil{
            let res = self.values!.sorted(by: {$0.key < $1.key})
            
            for current in res{
                items.append(CurrencyValue(date: current.key, value: current.value))
            }
            
            self.currentValue = items.last
            self.sliceItems()
        }
        
    }
    
    func sliceItems(){
        
        self.weekItems = Array(self.items[self.items.endIndex-8...self.items.endIndex-1])
        
        let yearElements = self.items.chunked(into: 31)
        
        for item in yearElements { //12 elements
            self.yearItems.append(ChocoHelper.current.getAverageFrom(items: item))
        }
        
        for item in yearElements.last!.chunked(into: 7){
            self.monthItems.append(ChocoHelper.current.getAverageFrom(items: item))
        }
        
    }
    
}
