//
//  Transaction.swift
//  ChocoBitcoin
//
//  Created by Dima on 11/16/18.
//  Copyright © 2018 fime. All rights reserved.
//

import Foundation
import ObjectMapper


public class Transaction: Mappable{
    
    var amountString : String?
    var dateTimestamp : String?
    var priceString : String?
    var tid : Int?
    var type : Int?
    
    var calculated: Double!
    var amount: Double!
    var price: Double!
    var date: String!
    
    required convenience public init?(map: Map) {
        self.init()
    }
    
    public func mapping(map: Map) {
        
        amountString <- map["amount"]
        dateTimestamp <- map["date"]
        priceString <- map["price"]
        tid <- map["tid"]
        type <- map["type"]
        
        self.calculate()
    }
    
    func calculate(){
        
        self.amount = Double(amountString!) ?? 0.0
        self.price = Double(priceString!) ?? 0.0
        self.calculated = amount*price
        self.date = ChocoHelper.current.timestampToFormattedString(value: self.dateTimestamp!)
        
        
    }
    
    
}
