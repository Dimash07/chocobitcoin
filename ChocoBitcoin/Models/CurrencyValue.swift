//
//  CurrencyValue.swift
//  ChocoBitcoin
//
//  Created by Dima on 11/15/18.
//  Copyright © 2018 fime. All rights reserved.
//

import Foundation

struct CurrencyValue {
    
    var date: String = ""
    var value: Double = 0.0
    
}
