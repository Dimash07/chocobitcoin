//
//  ChocoHelper.swift
//  ChocoBitcoin
//
//  Created by Dima on 11/15/18.
//  Copyright © 2018 fime. All rights reserved.
//

import Foundation
import UIKit


enum CurrenciesType: String, CaseIterable{
    case usd = "USD",
    eur = "EUR",
    kzt = "KZT"
}


class ChocoHelper{
    
    static let current = ChocoHelper()
    
    var defaultCrypto: String = "BTC"
    

    var currencies = [CurrenciesType: Currency]()
    
    
    
    func loadValues(_ completion: @escaping (_ result: Currency?) -> Void){
        
        let allCases = CurrenciesType.allCases
        var loadedCount = 0
        
        for current in allCases{
            
            var symbol: String{
                switch current{
                case .usd:
                    return "$"
                case .eur:
                    return "€"
                case .kzt:
                    return "₸"
                }
            }
            
            
            API.getCurrentPrice(currency: current) { res in
                if let result = res{
                    
                    
                    
                    self.currencies[current] = result
                    self.currencies[current]!.symbol = symbol
                    
                    
                    
                    loadedCount += 1
                    if loadedCount == allCases.count{
                        completion(self.currencies[.usd])
                    }
                    
                    
                }
            }
        }
        
    }
    
    
    func getAverageFrom(items: [CurrencyValue])->CurrencyValue{
        
        //var result = items[(items.count - 1) / 2] //If need middle element
        var result = items.last!
        
        var total: Double = 0.0
        
        for item in items{
            total += item.value
        }
        
        result.value = total/Double(items.count)
        
        
        return result
        
        
    }
    
    func timestampToFormattedString(value: String)->String{
        
        if let interval = TimeInterval(value){
            let date = Date(timeIntervalSince1970: interval)
            let dateFormatter = DateFormatter()
            dateFormatter.timeZone = TimeZone(abbreviation: "GMT")
            dateFormatter.locale = NSLocale.current
            dateFormatter.dateFormat = "dd-MM-yyyy HH:mm"
            let strDate = dateFormatter.string(from: date)
            
            return strDate
        }
        return ""
        
        
    }
    
}
