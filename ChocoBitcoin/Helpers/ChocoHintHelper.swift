//
//  ChocoHintHelper.swift
//  ChocoBitcoin
//
//  Created by Dima on 11/16/18.
//  Copyright © 2018 fime. All rights reserved.
//

import Foundation
import UIKit
import Instructions

struct HintObject{
    let text: String!
    let view: UIView!
    let oval: Bool!
    
    init(text: String, view: UIView, oval: Bool = false){
        self.text = text
        self.view = view
        self.oval = oval
    }
}

class HintHelper: NSObject, CoachMarksControllerDataSource, CoachMarksControllerDelegate {
    
    let coachMarksController = CoachMarksController()
    
    var target: UIViewController!
    var hints = [HintObject]()
    
    init(controller: UIViewController) {
        super.init()
        
        self.target = controller
        self.coachMarksController.dataSource = self
        self.coachMarksController.overlay.color = UIColor(rgbValue: 0x000000, alpha: 0.5)
        self.coachMarksController.overlay.allowTap = true
        
        
    }
    
    func start(_ viewId: String){
        
        let defaults = UserDefaults.standard
        
        if let _ = defaults.string(forKey: viewId){
            //introduced
        }
        else{
            if viewId != " "{
                defaults.set("introduced", forKey: viewId)
            }
            coachMarksController.start(on: self.target)
        }
    }
    
    func numberOfCoachMarks(for coachMarksController: CoachMarksController) -> Int {
        return self.hints.count
    }
    
    func coachMarksController(_ coachMarksController: CoachMarksController,
                              coachMarkAt index: Int) -> CoachMark {
        
        if self.hints[index].oval{
            return coachMarksController.helper.makeCoachMark(for: self.hints[index].view) {
                (frame: CGRect) -> UIBezierPath in
                // This will create an oval cutout a bit larger than the view.
                return UIBezierPath(ovalIn: frame.insetBy(dx: -4, dy: -4))
            }
        }
        return coachMarksController.helper.makeCoachMark(for: self.hints[index].view)
        
        
    }
    
    func coachMarksController(_ coachMarksController: CoachMarksController, coachMarkViewsAt index: Int, madeFrom coachMark: CoachMark) -> (bodyView: CoachMarkBodyView, arrowView: CoachMarkArrowView?) {
        
        
        let coachViews = coachMarksController.helper.makeDefaultCoachViews(withArrow: true, withNextText: false, arrowOrientation: coachMark.arrowOrientation)
        
        coachViews.bodyView.hintLabel.text = self.hints[index].text
        //coachViews.bodyView.nextLabel.text = "Далее"
        
        return (bodyView: coachViews.bodyView, arrowView: coachViews.arrowView)
    }
    
}
