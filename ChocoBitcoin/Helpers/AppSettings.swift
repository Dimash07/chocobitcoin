//
//  AppSettings.swift
//  ChocoBitcoin
//
//  Created by Dima on 11/15/18.
//  Copyright © 2018 fime. All rights reserved.
//

import Foundation
import UIKit

class AppSettings{
    
    static let current = AppSettings()
    
    let tint = UIColor(rgbValue: 0xFFFFFF)
    let inactiveTint = UIColor(rgbValue: 0x51566C)
    let background = UIColor(rgbValue: 0x434756)
    let barBackground = UIColor(rgbValue: 0x2C2F3C)
    
    let income = UIColor(rgbValue: 0x43D404)
    let sale = UIColor(rgbValue: 0xFF3636)
    

}
