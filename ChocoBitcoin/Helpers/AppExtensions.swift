//
//  AppExtensions.swift
//  ChocoBitcoin
//
//  Created by Dima on 11/15/18.
//  Copyright © 2018 fime. All rights reserved.
//

import Foundation
import UIKit



extension UIColor{
    
    convenience init(rgbValue: UInt, alpha: Double = 1) {
        self.init(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(alpha)
        )
    }
    
}


extension UIView{
    
    @IBInspectable var cornerRadius: CGFloat{
        get {
            return 0.0
        }
        set(cornerRadius) {
            self.layer.cornerRadius = cornerRadius
        }
    }
    
}


extension String{
    
    enum RegularExpressions: String {
        case phone = "^\\s*(?:\\+?(\\d{1,3}))?([-. (]*(\\d{3})[-. )]*)?((\\d{3})[-. ]*(\\d{2,4})(?:[-.x ]*(\\d+))?)\\s*$"
    }
    
    func isValid(regex: RegularExpressions) -> Bool {
        return isValid(regex: regex.rawValue)
    }
    
    func isValid(regex: String) -> Bool {
        let matches = range(of: regex, options: .regularExpression)
        return matches != nil
    }
    
    func onlyDigits() -> String {
        let filtredUnicodeScalars = unicodeScalars.filter{CharacterSet.decimalDigits.contains($0)}
        return String(String.UnicodeScalarView(filtredUnicodeScalars))
    }
    
    func makeCall() {
        if isValid(regex: .phone) {
            if let url = URL(string: "tel://+\(self.onlyDigits())"), UIApplication.shared.canOpenURL(url) {
                if #available(iOS 10, *) {
                    UIApplication.shared.open(url)
                } else {
                    UIApplication.shared.openURL(url)
                }
            }
        }
    }
    func openSite() {
        if let url = URL(string: self), UIApplication.shared.canOpenURL(url) {
            if #available(iOS 10, *) {
                UIApplication.shared.open(url)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
    }
}


extension Array {
    func chunked(into size: Int) -> [[Element]] {
        return stride(from: 0, to: count, by: size).map {
            Array(self[$0 ..< Swift.min($0 + size, count)])
        }
    }
}

extension Double {
    func currencyFormat(symbol: String = "")->String{
        let numberFormatter = NumberFormatter()
        numberFormatter.groupingSeparator = ","
        numberFormatter.groupingSize = 3
        numberFormatter.usesGroupingSeparator = true
        numberFormatter.decimalSeparator = "."
        numberFormatter.numberStyle = .decimal
        numberFormatter.maximumFractionDigits = 2
        return symbol+numberFormatter.string(from: self as NSNumber)!
    }
}



extension UIViewController{
    
    func setTitle(_ title: String, subtitle: String = ""){
        
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(color: AppSettings.current.barBackground), for: .default)
        self.navigationController?.navigationBar.backgroundColor = AppSettings.current.barBackground
        self.navigationController?.navigationBar.isTranslucent = false
        self.view.backgroundColor = AppSettings.current.background
        
        let navView = UIView(frame: CGRect(x: 0, y: 0, width: 250, height: 32))
        
        var marginTop = 0
        if subtitle != ""{
            let subTitleView = UILabel(frame: CGRect(x: 0, y: 10, width: 250, height: 32))
            subTitleView.font = UIFont.systemFont(ofSize: 12)
            subTitleView.text = subtitle
            subTitleView.textAlignment = .center
            subTitleView.textColor = AppSettings.current.tint
            subTitleView.alpha = 0.5
            navView.addSubview(subTitleView)
            
            marginTop = -10
        }
        
        let titleView = UILabel(frame: CGRect(x: 0, y: marginTop, width: 250, height: 32))
        titleView.font = UIFont.systemFont(ofSize: 18)
        titleView.text = title
        titleView.textAlignment = .center
        titleView.textColor = AppSettings.current.tint
        navView.addSubview(titleView)
        
        self.navigationController?.navigationBar.topItem?.title = " "
        self.navigationController?.navigationBar.tintColor = AppSettings.current.tint
        
        
        self.navigationItem.titleView = navView
        
    }
    
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    
    
}

extension UIImage {
    public convenience init?(color: UIColor, size: CGSize = CGSize(width: 1, height: 1)) {
        let rect = CGRect(origin: .zero, size: size)
        UIGraphicsBeginImageContextWithOptions(rect.size, false, 0.0)
        color.setFill()
        UIRectFill(rect)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        guard let cgImage = image?.cgImage else { return nil }
        self.init(cgImage: cgImage)
    }
}
