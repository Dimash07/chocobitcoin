//
//  ValuesViewController.swift
//  ChocoBitcoin
//
//  Created by Dima on 11/15/18.
//  Copyright © 2018 fime. All rights reserved.
//

import UIKit

class ValuesViewController: UIViewController {
    
    
    @IBOutlet weak var currentCryptoCurrencyLabel: UILabel!
    @IBOutlet weak var currentValueLabel: UILabel!
    
    @IBOutlet var currencyButtons: [UIButton]!
    @IBOutlet var graphicButtons: [UIButton]!
    
    @IBOutlet weak var chartView: ChartView!
    
    var currentCurrency: Currency!
    var loader: DMLoader!
    
    var type: Int = 0
    var hint: HintHelper!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        self.loader = DMLoader(view: self.view)
        self.view.backgroundColor = AppSettings.current.background
        
        selectButton(index: 0, buttons: self.graphicButtons)
        
        self.addHints()
        self.loadItems()
        
    }
    
    func addHints(){
        //add hints
        self.hint = HintHelper(controller: self)
        self.hint.hints.append(HintObject(text: "Выбранная валюта", view: self.currencyButtons[0]))
        self.hint.hints.append(HintObject(text: "Текущий курс биткоина в выбранной валюте", view: self.currentValueLabel))
        self.hint.hints.append(HintObject(text: "Тип отображения графика", view: self.graphicButtons[0]))
        self.hint.hints.append(HintObject(text: "Ну и собственно сам график", view: self.chartView))
        
    }
    
    
    
    func loadItems(){
        
        self.loader.start()
        ChocoHelper.current.loadValues { (values) in
            
            self.loader.stop()
            self.selectCurrency(index: 0)
            
            self.hint.start("mainPage")
            
        }
    }
    
    
    func selectCurrency(index: Int){
        var type: CurrenciesType!
        
        switch index {
        case 0:
            type = .usd
        case 1:
            type = .eur
        case 2:
            type = .kzt
        default:
            break
        }
        
        self.currentCurrency = ChocoHelper.current.currencies[type]
        self.prepareViews()
        
        selectButton(index: index, buttons: self.currencyButtons)
        
    }
    
    func selectButton(index: Int, buttons: [UIButton]){
        for (current, button) in buttons.enumerated(){
            if current == index{
                button.setTitleColor(AppSettings.current.tint, for: .normal)
            }
            else{
                button.setTitleColor(AppSettings.current.inactiveTint, for: .normal)
            }
        }
    }
    
    @IBAction func currencyTapped(_ sender: UIButton) {
        
        self.selectCurrency(index: sender.tag)
        
    }
    
    
    @IBAction func graphicButtonTapped(_ sender: UIButton) {
        
        self.type = sender.tag
        selectButton(index: sender.tag, buttons: self.graphicButtons)
        
        self.prepareViews()
        
    }

    
    
    func prepareViews(){
        
        self.currentValueLabel.text = self.currentCurrency.currentValue?.value.currencyFormat(symbol: currentCurrency.symbol)
        
        var items: [CurrencyValue]!
        
        switch self.type {
        case 0:
            items = self.currentCurrency.yearItems
        case 1:
            items = self.currentCurrency.monthItems
        case 2:
            items = self.currentCurrency.weekItems
        default:
            break
        }
        
        self.chartView.drawItems(items)
        
        
    }
    
    

}
