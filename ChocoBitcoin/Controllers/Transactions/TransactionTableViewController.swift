//
//  TransactionTableViewController.swift
//  ChocoBitcoin
//
//  Created by Dima on 11/16/18.
//  Copyright © 2018 fime. All rights reserved.
//

import UIKit

class TransactionTableViewController: UITableViewController {

    var current: Transaction!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.setTitle("Transaction", subtitle: "ID:\(current.tid!)")
        
        
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "transactionDetail", for: indexPath)
        
        switch indexPath.row {
        case 0:
            cell.textLabel?.text = "Type"
            cell.detailTextLabel?.text = (current.type == 1) ? "SELL" : "BUY"
        case 1:
            cell.textLabel?.text = "Amount"
            cell.detailTextLabel?.text = current.amountString!+" BTC"
        case 2:
            cell.textLabel?.text = "Price"
            cell.detailTextLabel?.text = current.price.currencyFormat(symbol: "$")
        case 3:
            cell.textLabel?.text = "Total price"
            cell.detailTextLabel?.text = current.calculated.currencyFormat(symbol: "$")
        case 4:
            cell.textLabel?.text = "Date"
            cell.detailTextLabel?.text = current.date
        default:
            break
        }
        
        return cell
        
    }
    
}
