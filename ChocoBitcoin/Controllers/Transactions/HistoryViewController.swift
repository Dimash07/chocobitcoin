//
//  HistoryViewController.swift
//  ChocoBitcoin
//
//  Created by Dima on 11/15/18.
//  Copyright © 2018 fime. All rights reserved.
//

import UIKit

class HistoryViewController: UITableViewController {
    
    var loader: DMLoader!
    var transactions = [Transaction]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.setTitle("Transactions")
        
        self.loader = DMLoader(view: self.view)
        
        
        self.getTransactions()
        
    }
    
    func getTransactions(){
        self.loader.start()
        API.getTransactions { (result) in
            self.loader.stop()
            
            if let res = result{
                self.transactions = res
                self.tableView.reloadData()
            }
        }
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.transactions.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let current = self.transactions[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: "transactionCell", for: indexPath) as! TransactionTableViewCell
        
        cell.setup(item: current)
        
        return cell
        
    }
    
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        self.performSegue(withIdentifier: "showTransaction", sender: self.transactions[indexPath.row])
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showTransaction"{
            let upcoming = segue.destination as! TransactionTableViewController
            upcoming.current = sender as? Transaction
        }
    }
    


}
