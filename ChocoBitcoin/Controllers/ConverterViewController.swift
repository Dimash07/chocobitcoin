//
//  ConverterViewController.swift
//  ChocoBitcoin
//
//  Created by Dima on 11/15/18.
//  Copyright © 2018 fime. All rights reserved.
//

import UIKit

class ConverterViewController: UIViewController {
    
    
    @IBOutlet var currencyButtons: [UIButton]!
    
    @IBOutlet weak var coinField: UITextField!
    @IBOutlet weak var currencyField: UITextField!
    
    @IBOutlet var contactButtons: [UIButton]!
    
    var currentCurrency: Currency!
    var loader: DMLoader!
    
    var hint: HintHelper!

    override func viewDidLoad() {
        super.viewDidLoad()

        self.setTitle("Converter")
        self.hideKeyboardWhenTappedAround()
        
        self.loader = DMLoader(view: self.view)
        
        coinField.addTarget(self, action: #selector(coinToCurrency), for: .editingChanged)
        currencyField.addTarget(self, action: #selector(currencyToCoin), for: .editingChanged)
        
        self.addHints()
        self.getCurrencies()
        
    }
    
    
    func addHints(){
        //add hints
        self.hint = HintHelper(controller: self)
        self.hint.hints.append(HintObject(text: "Сюда можно ввести сумму в биткоинах", view: self.coinField))
        self.hint.hints.append(HintObject(text: "Которая конвертируется в выбранную валюту", view: self.currencyButtons[0]))
        self.hint.hints.append(HintObject(text: "И покажется вот здесь вот", view: self.currencyField))
        self.hint.hints.append(HintObject(text: "Если есть вопросы, нажмите сюда, чтобы позвонить мне :)", view: self.contactButtons[0]))
        self.hint.hints.append(HintObject(text: "Ну или можете посмотреть моё портфолио", view: self.contactButtons[1]))
        
        
    }
    
    func getCurrencies(){
        if ChocoHelper.current.currencies.count == 0{
            self.loader.start()
            ChocoHelper.current.loadValues { (values) in
                self.loader.stop()
                self.selectCurrency(index: 0)
            }
        }
        else{
            self.selectCurrency(index: 0)
        }
    }
    
    func selectCurrency(index: Int){
        var type: CurrenciesType!
        
        switch index {
        case 0:
            type = .usd
        case 1:
            type = .eur
        case 2:
            type = .kzt
        default:
            break
        }
        
        self.currentCurrency = ChocoHelper.current.currencies[type]
        self.coinToCurrency()
        
        selectButton(index: index, buttons: self.currencyButtons)
        
        self.hint.start("converterView")
        
    }
    
    func selectButton(index: Int, buttons: [UIButton]){
        for (current, button) in buttons.enumerated(){
            if current == index{
                button.setTitleColor(AppSettings.current.tint, for: .normal)
            }
            else{
                button.setTitleColor(AppSettings.current.inactiveTint, for: .normal)
            }
        }
    }
    
    @IBAction func currencyTapped(_ sender: UIButton) {
        
        self.selectCurrency(index: sender.tag)
        
    }
    
    @objc func coinToCurrency(){
        if let value = Double(self.coinField.text!){
            self.currencyField.text = "\(value*self.currentCurrency.currentValue!.value)"
        }
    }
    @objc func currencyToCoin(){
        let price = 1.0/self.currentCurrency.currentValue!.value
        if let value = Double(self.currencyField.text!), value != 0.0{
            self.coinField.text = "\(value*price)"
        }
    }
    
    @IBAction func bottomButtonTapped(_ sender: UIButton) {
        
        switch sender.tag {
        case 0:
            "77083875753".makeCall()
        case 1:
            "http://fime.kz".openSite()
        default:
            break
        }
        
        
    }
    
}
