//
//  MainTabViewController.swift
//  ChocoBitcoin
//
//  Created by Dima on 11/15/18.
//  Copyright © 2018 fime. All rights reserved.
//

import UIKit

class MainTabViewController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()

        self.prepareTabs()
    }
    
    func prepareTabs(){
        
        self.removeTabbarItemsText()
        
        self.tabBar.tintColor = AppSettings.current.tint
        self.tabBar.unselectedItemTintColor = AppSettings.current.inactiveTint
        self.tabBar.barTintColor = AppSettings.current.barBackground
        self.tabBar.isTranslucent = false
        
        UITabBar.appearance().shadowImage = UIImage()
        UITabBar.appearance().backgroundImage = UIImage()
        
        self.tabBar.layer.shadowOffset = CGSize(width: 0, height: 0)
        self.tabBar.layer.shadowRadius = 2
        self.tabBar.layer.shadowColor = UIColor.black.cgColor
        self.tabBar.layer.shadowOpacity = 0.3
        
    }
    
    
    func removeTabbarItemsText() {
        
        if let items = self.tabBar.items {
            for item in items {
                item.title = ""
                item.imageInsets = UIEdgeInsets(top: 6, left: 0, bottom: -6, right: 0)
            }
        }
    }

}
